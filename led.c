/*This program turn on three green LEDs on Altera DE1 board
  Written by ductmk54
*/

/*See orpsoc-cores/systems/de1/data/wb_interconf.conf line 26*/
char* gpio_base = (char*) 0x91000000;

int main(){
	/*Set the GPIO to all out*/
	char dir = 0xff;
	*(gpio_base+1) = dir;

	/*Turn on three right LEDs: LEDG0, LEDG1, LEDG2*/
	*(gpio_base+0) = 0x07; //0x07 = 0b00000111
	while(1)
	return 0;
}
