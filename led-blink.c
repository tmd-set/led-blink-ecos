//Nhay LED 1s
#include <cyg/kernel/kapi.h>
#include <stdio.h>

#define NTHREADS 1
#define STACKSIZE 4096

char *(gpio_base) = (char*) 0x91000000;

static cyg_handle_t thread[NTHREADS];
static cyg_thread thread_obj[NTHREADS];
static char stack[NTHREADS][STACKSIZE];

static void alarm_prog(cyg_addrword_t data);

void cyg_user_start(void)
{
	cyg_thread_create(4, alarm_prog, (cyg_addrword_t) 0,
				"alarm_thread", (void*) stack[0],
				STACKSIZE, &thread[0], &thread_obj[0]);
	cyg_thread_resume(thread[0]);	
}

cyg_alarm_t test_alarm_func;

static void alarm_prog(cyg_addrword_t data){
	cyg_handle_t test_counterH, system_clockH, test_alarmH;
	cyg_tick_count_t ticks;
	cyg_alarm test_alarm;
	unsigned how_many_alarms = 0, prev_alarms = 0, tmp_how_many;

	system_clockH = cyg_real_time_clock();
	cyg_clock_to_counter(system_clockH, &test_counterH);
	cyg_alarm_create(test_counterH, test_alarm_func,
			   (cyg_addrword_t) &how_many_alarms,
			   &test_alarmH, &test_alarm);
	cyg_alarm_initialize(test_alarmH, cyg_current_time()+100, 100);
	
	for (;;) {
		ticks = cyg_current_time();
		printf("Time is %llu ms \n", ticks*10);

		cyg_scheduler_lock();
		tmp_how_many = how_many_alarms;
		cyg_scheduler_unlock();
		if (prev_alarms != tmp_how_many) {
			printf(" ---> alarms call so far: %u\n", tmp_how_many);
			prev_alarms = tmp_how_many;
			if((tmp_how_many%2) == 0) {
				*(gpio_base+0) = 0xff; //LEDs on
				printf("ON\n");
			} else {
				*(gpio_base+0) = 0x00; //LEDs off
				printf("OFF\n");
			}
		}
		cyg_thread_delay(10);
	}
}

void test_alarm_func(cyg_handle_t alarmH, cyg_addrword_t data)
{
	*(gpio_base+1) = 0xff;
	++*((unsigned *) data);
}
