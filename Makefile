PREFIX = /home/duc/or/ecos/work/install
COMMAND_PREFIX = or1k-elf-
                                        
CC = $(COMMAND_PREFIX)gcc
CFLAGS = -Wall -g -O2 -nostdlib
IFLAGS = -I$(PREFIX)/include
LFLAGS = -L$(PREFIX)/lib
TFLAGS = -T$(PREFIX)/lib/target.ld
SRC = led.c
TARGET = led.elf

$(TARGET) : $(SRC)
		$(CC) $(CFLAGS) $(IFLAGS) $(LFLAGS) $(TFLAGS) $(SRC) -o $(TARGET)

led-blink:
	$(CC) $(CFLAGS) $(IFLAGS) $(LFLAGS) $(TFLAGS) led-blink.c -o led-blink.elf

led-counter:
	$(CC) $(CFLAGS) $(IFLAGS) $(LFLAGS) $(TFLAGS) led-counter.c -o led-counter.elf

led-counter2:
	$(CC) $(CFLAGS) $(IFLAGS) $(LFLAGS) $(TFLAGS) led-counter2.c -o led-counter2.elf

clean: 
		rm -rf *.elf

